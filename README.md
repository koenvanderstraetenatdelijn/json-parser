# JsonLogParser

## About
Used to format JSON logging to make it more readable in a local text file.

## How to
1. Paste the JSON log in `src/main/resources/log.json`.
2. Run method `LogReader#main()`