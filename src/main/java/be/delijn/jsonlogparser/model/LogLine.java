package be.delijn.jsonlogparser.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class LogLine {
    private String type;
    private String host;
    private String ibm_userDir;
    private String ibm_serverName;
    private String message;
    private String ibm_threadId;
    private String ibm_datetime;
    private String ibm_messageId;
    private String module;
    private String loglevel;
    private String ibm_methodName;
    private String ibm_className;
    private String ibm_sequence;
    private String ext_thread;

    public String getType() {
        return type;
    }

    public String getHost() {
        return host;
    }

    public String getIbm_userDir() {
        return ibm_userDir;
    }

    public String getIbm_serverName() {
        return ibm_serverName;
    }

    public String getMessage() {
        return message;
    }

    public String getIbm_threadId() {
        return ibm_threadId;
    }

    public String getIbm_datetime() {
        return ibm_datetime;
    }

    public String getIbm_messageId() {
        return ibm_messageId;
    }

    public String getModule() {
        return module;
    }

    public String getLoglevel() {
        return loglevel;
    }

    public String getIbm_methodName() {
        return ibm_methodName;
    }

    public String getIbm_className() {
        return ibm_className;
    }

    public String getIbm_sequence() {
        return ibm_sequence;
    }

    public String getExt_thread() {
        return ext_thread;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, org.apache.commons.lang3.builder.ToStringStyle.MULTI_LINE_STYLE)
                .append("type", type)
                .append("host", host)
                .append("ibm_userDir", ibm_userDir)
                .append("ibm_serverName", ibm_serverName)
                .append("message", message)
                .append("ibm_threadId", ibm_threadId)
                .append("ibm_datetime", ibm_datetime)
                .append("ibm_messageId", ibm_messageId)
                .append("module", module)
                .append("loglevel", loglevel)
                .append("ibm_methodName", ibm_methodName)
                .append("ibm_className", ibm_className)
                .append("ibm_sequence", ibm_sequence)
                .append("ext_thread", ext_thread)
                .toString();
    }
}
