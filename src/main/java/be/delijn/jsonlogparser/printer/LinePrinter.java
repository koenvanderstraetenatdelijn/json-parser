package be.delijn.jsonlogparser.printer;

import java.util.List;
import java.util.function.Supplier;

public class LinePrinter {
    public void printLine(List<Supplier<String>> getters) {
        boolean printedAtLeast1Line = false;
        for (Supplier<String> getter : getters) {
            String line = getter.get();

            boolean isLoggableOnBreadcrumb = line.startsWith("[ entering < onBreadCrumb")
                    || line.startsWith("[ leaving < onBreadCrumb");
            if (!isLoggableOnBreadcrumb) {
                printedAtLeast1Line = true;
                System.out.println(line);
            }
        }

        if (printedAtLeast1Line) {
            System.out.print("\n\n");
        }
    }
}
