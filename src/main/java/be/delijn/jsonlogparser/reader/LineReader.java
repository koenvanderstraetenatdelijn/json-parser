package be.delijn.jsonlogparser.reader;

import be.delijn.jsonlogparser.model.LogLine;
import com.google.gson.Gson;

public class LineReader {
    public LogLine readLine(String jsonLine) {
        Gson g = new Gson();
        return g.fromJson(jsonLine, LogLine.class);
    }
}
