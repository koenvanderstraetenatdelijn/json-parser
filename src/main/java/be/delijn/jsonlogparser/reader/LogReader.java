package be.delijn.jsonlogparser.reader;

import be.delijn.jsonlogparser.model.LogLine;
import be.delijn.jsonlogparser.printer.LinePrinter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static java.util.Collections.singletonList;

public class LogReader {
    private static final String RESOURCES_DIR = "C:\\Users\\mxtvk\\Projects\\AAA TEMP\\JsonLogParser\\src\\main\\resources";

    private LineReader lineReader;
    private LinePrinter linePrinter;

    public LogReader() {
        this.lineReader = new LineReader();
        this.linePrinter = new LinePrinter();
    }

    private void readJsonLog() {
        String fileName = RESOURCES_DIR + "/log.json";

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach(s -> {
                LogLine logLine = lineReader.readLine(s);
                if (logLine != null) {
                    linePrinter.printLine(singletonList(logLine::getMessage));
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new LogReader().readJsonLog();
    }
}
